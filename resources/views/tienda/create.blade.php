<h1>Formulario de Tienda</h1>

<form action="{{ url('/tiendas') }}" method="post" enctype="multipart/form-data">
    @csrf
    <label for="nombre">Nombre</label>
    <input type="text" name="nombre" id="nombre">
    <br>
    <label for="direccion">Dirección</label>
    <input type="text" name="direccion" id="direccion">
    <br>
    <label for="telefono">Teléfono</label>
    <input type="text" name="telefono" id="telefono">
    <br>
    <label for="email">Correo Electrónico</label>
    <input type="text" name="email" id="email">
    <br>
    <label for="mapa">Ubicación</label>
    <input type="text" name="mapa" id="mapa">
    <br>
    <label for="imagen">Imagen</label>
    <input type="file" name="imagen" id="imagen">
    <br>
    <input type="submit" value="Guardar Datos">
    <a href="{{ url('tiendas/') }}">Regresar</a>


</form>