<h1>Listado de Tiendas</h1>
@if(Session::has('mensaje'))
{{Session::get('mensaje')}}
@endif
<a href="{{ url('tiendas/create') }}">Registrar tiendas</a>
<table class="table table-light">
	<thead class="thead-light">
		<tr>
			<th>#</th>
			<th>Nombre</th>
			<th>Dirección</th>
			<th>Teléfono</th>
			<th>Correo Electrónico</th>
			<th>Ubicación</th>
			<th>Imagen</th>
		</tr>
	</thead>
	<tbody>
		@foreach($tiendas as $tienda)
		<tr>
			<td>{{$tienda->id}}</td>
			<td>{{$tienda->nombre}}</td>
			<td>{{$tienda->direccion}}</td>
			<td>{{$tienda->telefono}}</td>
			<td>{{$tienda->email}}</td>
			<td>{{$tienda->mapa}}</td>
			<td><img src="{{asset('storage').'/'.$tienda->imagen}}" alt="" width="80" height="80">	</td>
		</tr>
		@endforeach
	</tbody>
</table>